package com.ddm.product.controller;

import java.util.Collections;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ddm.product.dto.UserPrincipal;
import com.ddm.product.entity.UserInfoEntity;
import com.ddm.product.exception.ResourceNotFoundException;
import com.ddm.product.repository.UserRepository;
import com.ddm.product.security.CurrentUser;

@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/user/me")
    @PreAuthorize("hasRole('USER')")
    public UserInfoEntity getCurrentUser(@CurrentUser UserPrincipal userPrincipal) {
        return userRepository.findByUserUniqueId(userPrincipal.getUserId())
                .orElseThrow(() -> new ResourceNotFoundException("User", "id", userPrincipal.getUserId()));
    }
    
    @GetMapping("/user/name")
    public Map<String, String> getUserName(@CurrentUser UserPrincipal userPrincipal) {
        return Collections.singletonMap("name", userRepository.findByUserUniqueId(userPrincipal.getUserId()).get().getName());
    }
}