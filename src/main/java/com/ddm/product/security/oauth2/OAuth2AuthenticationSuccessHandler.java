package com.ddm.product.security.oauth2;

import java.io.IOException;
import java.util.Collections;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.ddm.product.utils.JWTUtility;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class OAuth2AuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    private JWTUtility jWTUtility;

    private final ObjectMapper mapper;

    @Autowired
    OAuth2AuthenticationSuccessHandler(JWTUtility jWTUtility, ObjectMapper mapper) {
        this.jWTUtility = jWTUtility;
        this.mapper = mapper;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
    	String token = jWTUtility.createToken(authentication);
    	response.getWriter().write(mapper.writeValueAsString(Collections.singletonMap("accessToken", token)));
    }
}
