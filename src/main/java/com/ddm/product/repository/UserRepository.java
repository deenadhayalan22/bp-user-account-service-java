package com.ddm.product.repository;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ddm.product.entity.UserInfoEntity;

@Repository
public interface UserRepository extends JpaRepository<UserInfoEntity, Long> {

    Optional<UserInfoEntity> findByEmail(String email);
    
    Optional<UserInfoEntity> findByUserUniqueId(UUID userUniqueId);

    Boolean existsByEmail(String email);

}
