package com.ddm.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BudgetPlannerJavaUserAccountServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(BudgetPlannerJavaUserAccountServiceApplication.class, args);
	}

}
