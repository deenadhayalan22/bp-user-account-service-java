package com.ddm.product.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

import com.ddm.product.security.RestAuthenticationEntryPoint;
import com.ddm.product.security.TokenAuthenticationFilter;
import com.ddm.product.security.oauth2.CustomOAuth2UserService;
import com.ddm.product.security.oauth2.InMemoryRequestRespository;
import com.ddm.product.security.oauth2.OAuth2AuthenticationFailureHandler;
import com.ddm.product.security.oauth2.OAuth2AuthenticationSuccessHandler;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        securedEnabled = true,
        jsr250Enabled = true,
        prePostEnabled = true
)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private CustomOAuth2UserService customOAuth2UserService;

    @Autowired
    private OAuth2AuthenticationSuccessHandler oAuth2AuthenticationSuccessHandler;

    @Autowired
    private OAuth2AuthenticationFailureHandler oAuth2AuthenticationFailureHandler;
    
    @Autowired
    private InMemoryRequestRespository inMemoryRequestRespository;
    
    @Autowired
    private TokenAuthenticationFilter tokenAuthenticationFilter;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
		http.cors()
				.and()
					.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
				.and().csrf().disable().formLogin().disable().httpBasic().disable()
					.exceptionHandling()
					.authenticationEntryPoint(new RestAuthenticationEntryPoint())
				.and()
					.authorizeRequests().antMatchers("/oauth2/**", "/login**").permitAll().anyRequest().authenticated()
				.and()
					.oauth2Login().authorizationEndpoint()
					.authorizationRequestRepository(inMemoryRequestRespository)
				.and()
					.userInfoEndpoint().userService(customOAuth2UserService)
				.and()
					.successHandler(oAuth2AuthenticationSuccessHandler)
					.failureHandler(oAuth2AuthenticationFailureHandler);

        // Add our custom Token based authentication filter
        http.addFilterBefore(tokenAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);
    }
}
