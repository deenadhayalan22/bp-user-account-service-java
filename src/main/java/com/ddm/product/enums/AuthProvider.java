package com.ddm.product.enums;

public enum AuthProvider {
	local,
    facebook,
    google,
    github
}
